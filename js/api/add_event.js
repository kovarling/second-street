var btnCust = '<div class="form-group"><div class="checkbox"><label>'+
    '<input type="checkbox" name="" value="1">Overwrite files</label></div></div>';

function parse(val) {
    var result = "Not found",
        tmp = [];
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] === val) result = decodeURIComponent(tmp[1]);
    }
    return result;
}

function fillEventForm(obj){
    var form = $('#event-form');
    form.find('input:not(:submit), textarea').each(function () {

        switch ($(this).attr('name')){
            case "photo": break;
            case "date":case "date_end":case "post_date":
                var date = new Date(obj[$(this).attr('name')]);
                $(this).parent().data("DateTimePicker").date(date.getMonth()+1+
                    "/"+date.getDate()+
                    "/"+date.getFullYear()+
                    " "+date.getHours()+":"+date.getMinutes()
                ); break;
            case "active": if(obj.status == '1'){
                $(this).prop('checked', true);
            } else {
                $(this).prop('checked', false);
            } break;
            default: $(this).val(obj[$(this).attr('name')]);
        }
    });
    $("#avatar").fileinput('destroy');
    $("#avatar").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="'+ obj.photo +'" alt="Your Avatar" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
}

function putEventSubmit(){

    $.ajax({
        type: "POST",
        url: "../js/json/events/update-event.json",
        data: $('#event-form').serialize(),
        success: function (data) {
            if (data['success']){
                $.jGrowl(data['success'], {life: 10000, theme: 'green'})
            } else {
                $.jGrowl(data['error'], {life: 10000, theme: 'red'})
            }
        }
    });
}

$(function () {
    $('.datetimepicker').datetimepicker();

    $("#avatar").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="/img/default-male.png" alt="Your Avatar" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
    var id = parse('id');
    if (id != "Not found"){
        $.ajax({
            type: "GET",
            url: "../js/json/events/event.json",
            success: function (data) {
                if (data.id){
                    fillEventForm(data);
                } else {
                    $.jGrowl('User not found', {life: 10000, theme: 'red'})
                }
            }
        });
    }


});