var tr = [];
var table = $('#users-table');
function forEach(data){
    for(var key in data){
        if(data.hasOwnProperty(key)){
            forEachObj(data[key]);
        }
    }
}

function forEachObj(obj){
    var active;
    if (obj.active == "1"){
        active = "green";
    } else {
        active = "red";
    }
    var verified = "";
    if (obj.verified == "1"){
        verified = "checked";
    }
    tr.push("<tr role='row' " +
        "data-id=" + obj.id + " " +
        "data-username=" + obj.username + " " +
        "data-email=" + obj.email + " " +
        "data-gender=" + obj.gender + " " +
        "data-city=" + obj.city + " " +
        "data-age=" + obj.age + " " +
        "data-first_name=" + obj.first_name + " " +
        "data-last_name=" + obj.last_name + " " +
        "data-photo=" + obj.photo + " " +
        "data-active=" + obj.active + " " +
        "data-verified=" + obj.verified + " " +
        ">" +
        "<td class='username'>" + obj.username +
        "</td>" +
        "<td class='email'>" + obj.email +
        "</td>" +
        "<td class='gender'>" + obj.gender +
        "</td>" +
        "<td class='city'>" + obj.city +
        "</td>" +
        "<td class='age'>" + obj.age +
        "</td>" +
        "<td class='first_name'>" + obj.first_name +
        "</td>" +
        "<td class='last_name'>" + obj.last_name +
        "</td>" +
        "<td class='photo'><img src='" + obj.photo + "'>" +
        "</td>" +
        "<td class='user-active'><i class='fa fa-circle " + active + "'></i>" +
        "</td>" +
        '<td class="actions">' +
        '<span class="user-update"><i class="fa fa-edit"></i></span>' +
        '<span class="user-delete"><i class="fa fa-remove"></i></span></td>'+
        '<td><div class="checkbox"><input value="1" type="checkbox" '+ verified +'  name="verified"></div></td>'+
        "</tr>");
}

function deleteUser(id){
    var data = {
        id : id
    };
    $.ajax({
        dataType: "json",
        type: "POST",
        url: "../js/json/users/del-user.json",
        data: data,
        success: function (data) {
            if (data['success']){
                $.jGrowl(data['success'], {life: 10000, theme: 'green'});
                $('tr[data-id="'+id+'"').remove();
            } else {
                $.jGrowl(data['error'], {life: 10000, theme: 'red'});
            }
        }
    });

}
var user;
function putUser(id){
    user = $('tr[data-id="'+id+'"');
    $('#form-update-user input:not(:submit)').each(function () {

        switch ($(this).attr('name')){
            case "photo": break;
            case "active": if(user.data('active') == '1'){
                $(this).prop('checked', true);
            } else {
                $(this).prop('checked', false);
            } break;
            default: $(this).val(user.data($(this).attr('name')));
        }
    });
    $('#form-update-user select[name="gender"]').val(user.data('gender'));
    $('#updateUser').modal('show');
    $("#avatar").fileinput('destroy');
    $("#avatar").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="'+ user.data('photo') +'" alt="Your Avatar" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });

}
function putUserSubmit(){

    $.ajax({
        type: "POST",
        url: "../js/json/users/update-user.json",
        data: $('#updateUser').find('form').serialize(),
        success: function (data) {
            if (data['success']){
                $.jGrowl(data['success'], {life: 10000, theme: 'green'})
                setTimeout(function () {
                    window.location.reload();
                }, 2000)
            } else {
                $.jGrowl(data['error'], {life: 10000, theme: 'red'})
            }
        }
    });
}

function verifyUser(id){
    var v = $('tr[data-id="'+id+'"');
    var data = {
        id : id,
        verified : !v.data('verified')
    };
    $.ajax({
        type: "POST",
        data : data,
        url: "../js/json/users/verify-user.json",
        success: function (data) {
            if (data['success']){
                $.jGrowl(data['success'], {life: 10000, theme: 'green'});
                v.data('verified', !v.data('verified'))
            } else {
                $.jGrowl(data['error'], {life: 10000, theme: 'red'})
            }
        }
    });
}

$(function () {
    $.ajax({
        type: "GET",
        url: "../js/json/users/users.json",
        success: function (data) {
            if (data['users']){
                forEach(data['users']);
            } else {
                console.log('error');
            }
            table.find('tbody').html(tr.join());
            table.DataTable({
                responsive: true
            });
        }
    });

    table.on('click','.user-update', function () {
        putUser($(this).parents('tr').data('id'));
    });
    table.on('click','.user-delete', function () {
        deleteUser($(this).parents('tr').data('id'));
    });
    table.on('change','[name="verified"]', function () {
        verifyUser($(this).parents('tr').data('id'));
    });

});