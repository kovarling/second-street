var tr = [];
var table = $('#events-table');
function forEach(data){
    for(var key in data){
        if(data.hasOwnProperty(key)){
            forEachObj(data[key]);
        }
    }
}

function forEachObj(obj){
    var active;
    if (obj.status == "1"){
        active = "green";
    } else {
        active = "red";
    }
    tr.push("<tr role='row' " +
        "data-id=" + obj.id + " " +
        "data-date=" + obj.date + " " +
        "data-status=" + obj.status + " " +
        "data-venue=" + obj.venue + " " +
        "data-photo=" + obj.photo + " " +
        "data-name=" + obj.name + " " +
        ">" +
        "<td class='date'>" + obj.date +
        "</td>" +
        "<td class='name'>" + obj.name +
        "</td>" +
        "<td class='user-active'><i class='fa fa-circle " + active + "'></i>" +
        "<td class='venue'>" + obj.venue +
        "</td>" +
        "<td class='photo'><img src='" + obj.photo + "'>" +
        "</td>" +
        "</td>" + '<td class="actions"><span class="event-update"><i class="fa fa-edit"></i></span><span class="event-delete"><i class="fa fa-remove"></i></span></td>'+
        "</tr>");
}

function putEvent(id){
    window.location.href = "http://"+window.location.hostname + "/templates/add_event.html?id="+id;
}
function deleteEvent(id){
    var data = {
        id : id
    };
    $.ajax({
        dataType: "json",
        type: "POST",
        url: "../js/json/Events/del-event.json",
        data: data,
        success: function (data) {
            if (data['success']){
                $.jGrowl(data['success'], {life: 10000, theme: 'green'});
                $('tr[data-id="'+id+'"').remove();
            } else {
                $.jGrowl(data['error'], {life: 10000, theme: 'red'});
            }
        }
    });
}

$(function () {
    $.ajax({
        type: "GET",
        url: "../js/json/events/events.json",
        success: function (data) {
            if (data['events']){
                forEach(data['events']);
            } else {
                console.log('error');
            }
            table.find('tbody').html(tr.join());
            table.DataTable({
                responsive: true
            });
        }
    });

    table.on('click','.event-update', function () {
        putEvent($(this).parents('tr').data('id'));
    });
    table.on('click','.event-delete', function () {
        deleteEvent($(this).parents('tr').data('id'));
    });
});